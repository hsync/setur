﻿using Microsoft.Extensions.Hosting;
using Setur.Core.Event;
using Setur.ViewModel.Events;

namespace Setur.PostgresConsumer
{
    public class PostgresConsumerHostedService : IHostedService
    {
        private readonly IConsumer _consumer;

        public PostgresConsumerHostedService(IConsumer consumer)
        {
            _consumer = consumer;
        }
        public Task StartAsync(CancellationToken cancellationToken)
        {
            return _consumer.ConsumeAsync<HotelCreateEventData>(topicName: "HOTEL_CREATE", async eventData => await SaveToDbAsync(eventData), groupId: "setur_postgres", cancellationToken);
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        public Task SaveToDbAsync(HotelCreateEventData eventData)
        {
            eventData.Attributes.TryGetValue("hotelName", out var hotelName);
            Console.WriteLine($"Hotel save to postgres db App: {eventData.App} HotelName {hotelName}");
            return Task.CompletedTask;
        }
    }
}

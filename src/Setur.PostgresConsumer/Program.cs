﻿// See https://aka.ms/new-console-template for more information
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Setur.PostgresConsumer;

Console.WriteLine("Postgres Consumer Started");
Host.CreateDefaultBuilder()
    .ConfigureServices(services =>
    {
        services.AddKafkaConsumer();
        services.AddHostedService<PostgresConsumerHostedService>();
        //services.AddDbContext<SocketDbContext>(options =>
        //{
        //    options.UseSqlServer("Server=.;Database=socketDb;Trusted_Connection=True;MultipleActiveResultSets=true; TrustServerCertificate=True", b => b.MigrationsAssembly("SBUygulama.EntityFrameworkCore"));
        //});
    }).Build().Run();


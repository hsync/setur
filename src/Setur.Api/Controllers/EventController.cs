using Microsoft.AspNetCore.Mvc;
using Setur.Service.HotelCreate;
using Setur.ViewModel;

namespace Setur.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class EventController : ControllerBase
    {

        private readonly ILogger<EventController> _logger;
        private readonly IHotelCreateService _hotelCreateService;

        public EventController(ILogger<EventController> logger, IHotelCreateService hotelCreateService)
        {
            _logger = logger;
            _hotelCreateService = hotelCreateService;
        }

        [HttpPost]
        public Task Post(HotelCreateEventListDto hotelCreateEventList)
        {
            return _hotelCreateService.PublishEventsAsync(hotelCreateEventList);
        }
    }
}
﻿namespace Setur.Kafka
{
    public class KafkaOptions
    {
        public string BootstrapServers { get; set; }
        public string SaslPassword { get; set; }
    }
}

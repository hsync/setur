﻿using Setur.Core.Event;
using Setur.Kafka;
using Setur.Kafka.Consumer;
using Setur.Kafka.Producer;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class KafkaServiceCollectionExtension
    {
        public static IServiceCollection AddKafkaProducer(this IServiceCollection services)
        {
            services.AddKafka();
            services.AddTransient<IProducer, KafkaProducer>();
            return services;
        }

        public static IServiceCollection AddKafkaConsumer(this IServiceCollection services)
        {
            services.AddKafka();
            services.AddSingleton<IConsumer, KafkaConsumer>();
            return services;
        }

        private static IServiceCollection AddKafka(this IServiceCollection services)
        {
            services.Configure<KafkaOptions>(options =>
            {
                options.BootstrapServers = Environment.GetEnvironmentVariable("KAFKA_BOOTSTRAP_SERVERS");
                //options.SaslPassword = Environment.GetEnvironmentVariable("KafkaSaslPassword");
            });
            return services;
        }
    }
}
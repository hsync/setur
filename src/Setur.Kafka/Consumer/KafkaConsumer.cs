﻿using Confluent.Kafka;
using Microsoft.Extensions.Options;
using Setur.Core.Event;
using System.Text.Json;

namespace Setur.Kafka.Consumer
{
    public class KafkaConsumer : IConsumer
    {
        private readonly KafkaOptions _options;

        public KafkaConsumer(IOptions<KafkaOptions> options)
        {
            _options = options.Value;
        }
        public async Task ConsumeAsync<TEvent>(string topicName, Action<TEvent> eventHandler, string groupId = "setur", CancellationToken cancellationToken = default) where TEvent : class
        {
            var config = new ConsumerConfig
            {
                GroupId = groupId,
                BootstrapServers = _options.BootstrapServers,
                SaslPassword = _options.SaslPassword,
                AutoOffsetReset = AutoOffsetReset.Earliest,

            };

            using (var consumer = new ConsumerBuilder<Ignore, string>(config).SetValueDeserializer(Deserializers.Utf8).Build())
            {
                consumer.Subscribe(topicName);

                while (!cancellationToken.IsCancellationRequested)
                {
                    ConsumeResult<Ignore, string> consumeResult = consumer.Consume(cancellationToken);
                    if (consumeResult.Message.Value != null)
                    {
                        var hsyncEvent = JsonSerializer.Deserialize<TEvent>(consumeResult.Message.Value);
                        if (hsyncEvent != null)
                        {
                            eventHandler(hsyncEvent);
                            //Console.WriteLine($"Received message at {consumeResult.TopicPartitionOffset}: {hsyncEvent}");
                            consumer.Commit(consumeResult);
                        }
                    }

                }
            }
            await Task.CompletedTask;
        }

    }
}

﻿using Confluent.Kafka;
using Microsoft.Extensions.Options;
using Setur.Core.Event;
using System.Text.Json;

namespace Setur.Kafka.Producer
{
    public class KafkaProducer : IProducer
    {
        private readonly KafkaOptions _options;

        public KafkaProducer(IOptions<KafkaOptions> options)
        {
            _options = options.Value;
        }

        public async Task PublishAsync<TEvent>(string topicName, TEvent data) where TEvent : class
        {
            //var topicName = "commands5";
            //var kafkaUrl = "192.168.1.8";

            var config = new ProducerConfig()
            {
                BootstrapServers = _options.BootstrapServers,
                SaslPassword = _options.SaslPassword
            };

            using (var producer = new ProducerBuilder<Null, string>(config).SetValueSerializer(Serializers.Utf8).Build())
            {

                var messageValue = JsonSerializer.Serialize(data);
                var message = new Message<Null, string> { Value = messageValue };
                DeliveryResult<Null, string> result = await producer.ProduceAsync(topicName, message);
            }
        }

    }
}

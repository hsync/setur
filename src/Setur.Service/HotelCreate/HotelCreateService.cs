﻿using Microsoft.Extensions.Logging;
using Setur.Core.Event;
using Setur.ViewModel;
using Setur.ViewModel.Events;

namespace Setur.Service.HotelCreate
{
    public class HotelCreateService : IHotelCreateService
    {
        private readonly IProducer _producer;
        private readonly ILogger<HotelCreateService> _logger;

        public HotelCreateService(IProducer producer, ILogger<HotelCreateService> logger)
        {
            _producer = producer;
            _logger = logger;
        }

        public async Task PublishEventsAsync(HotelCreateEventListDto hotelCreateEventList)
        {
            _logger.LogInformation("Publishing Events");
            if (hotelCreateEventList == null || hotelCreateEventList.Events == null || !hotelCreateEventList.Events.Any())
            {
                _logger.LogWarning("No events to publish");
                return;
            }

            var mappedEvents = hotelCreateEventList.Events.Select(x => new HotelCreateEventData
            {
                App = x.App,
                Type = x.Type,
                Time = x.Time,
                IsSucceeded = x.IsSucceeded,
                Meta = new() { },
                User = new()
                {
                    Id = x.User.Id,
                    Provider = x.User.Provider,
                    Email = x.User.Email,
                    IsAuthenticated = x.User.IsAuthenticated
                },
                Attributes = x.Attributes
            }).ToList();
            foreach (var hotelEvent in mappedEvents)
            {
                await _producer.PublishAsync(hotelEvent.Type, hotelEvent);
            }
        }
    }
}

﻿using Setur.ViewModel;

namespace Setur.Service.HotelCreate
{
    public interface IHotelCreateService
    {
        Task PublishEventsAsync(HotelCreateEventListDto hotelCreateEventList);
    }
}
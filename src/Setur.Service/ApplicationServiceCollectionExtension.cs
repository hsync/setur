﻿using Setur.Service.HotelCreate;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ApplicationServiceCollectionExtension
    {
        public static IServiceCollection AddApplicationServices(this IServiceCollection services)
        {
            services.AddKafkaProducer();
            services.AddTransient<IHotelCreateService, HotelCreateService>();
            return services;
        }
    }
}
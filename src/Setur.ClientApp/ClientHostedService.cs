﻿using Microsoft.Extensions.Hosting;
using System.Text;
using System;
using System.Text.Json;
using System.Text.Json.Serialization;
using Setur.ViewModel;
using Microsoft.Extensions.Logging;

namespace Setur.ClientApp
{
    public class ClientHostedService : IHostedService
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public ClientHostedService(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }
        public async Task StartAsync(CancellationToken cancellationToken)
        {
            var httpClient = _httpClientFactory.CreateClient("Setur");
            var request = new HotelCreateEventListDto();
            var list = new List<HotelCreateEventDto>();
            var i = 0;
            do
            {
                list.Add(new HotelCreateEventDto
                {
                    App = Guid.NewGuid().ToString(),
                    Type = "HOTEL_CREATE",
                    Time = DateTime.Now,
                    IsSucceeded = true,
                    User = new()
                    {
                        Id = Guid.NewGuid().ToString(),
                        Provider = "b2c-internal",
                        Email = "setur@setur.com.tr",
                        IsAuthenticated = i % 2 == 0,
                    },
                    Attributes = new Dictionary<string, string> { { "hotelId", $"412{i}" }, { "hotelRegion", "Adıyaman" }, { "hotelName", "Nemrut" } }

                });
                request.Events = list;
                var json = JsonSerializer.Serialize(request);
                var data = new StringContent(json, Encoding.UTF8, "application/json");
                try
                {
                    using (var response = await httpClient.PostAsync("/event", data))
                    {
                        response.EnsureSuccessStatusCode();
                    }

                }
                catch (Exception ex)
                {

                    Console.WriteLine("ERR:" + ex.InnerException?.Message ?? ex.Message);
                }
                await Task.Delay(1000);

                i++;

            } while (!cancellationToken.IsCancellationRequested);
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }
}

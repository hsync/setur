﻿// See https://aka.ms/new-console-template for more information
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Setur.ClientApp;

Console.WriteLine("Client App Started");

Host.CreateDefaultBuilder()
    .ConfigureServices(services =>
    {
        services.AddHttpClient("Setur", client =>
        {
            client.BaseAddress = new Uri(Environment.GetEnvironmentVariable("API_URL"));
        });

        services.AddHostedService<ClientHostedService>();

    }).Build().Run();
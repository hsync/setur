﻿namespace Setur.Core.Event
{
    public interface IProducer
    {
        Task PublishAsync<TEvent>(string topicName, TEvent data) where TEvent : class;
    }
}
﻿namespace Setur.Core.Event
{
    public interface IConsumer
    {
        Task ConsumeAsync<TEvent>(string topicName, Action<TEvent> eventHandler, string groupId = "setur", CancellationToken cancellationToken = default) where TEvent : class;
    }
}
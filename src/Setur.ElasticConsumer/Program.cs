﻿// See https://aka.ms/new-console-template for more information
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Setur.ElasticConsumer;

Console.WriteLine("Elastic Consumer Started");
Host.CreateDefaultBuilder()
    .ConfigureServices(services =>
    {
        services.AddKafkaConsumer();
        services.AddHostedService<ElasticConsumerHostedService>();
    }).Build().Run();

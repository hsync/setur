﻿namespace Setur.ViewModel.Events
{
    public class HotelCreateEventData
    {
        public string App { get; set; }
        public string Type { get; set; }
        public DateTime Time { get; set; }
        public bool IsSucceeded { get; set; }
        public HotelMetaEventArg Meta { get; set; }
        public HotelUserEventArg User { get; set; }
        public Dictionary<string, string> Attributes { get; set; }
    }
}

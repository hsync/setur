﻿namespace Setur.ViewModel.Events
{
    public class HotelUserEventArg
    {
        public string Id { get; set; }
        public bool IsAuthenticated { get; set; }
        public string Provider { get; set; }
        public string Email { get; set; }
    }
}

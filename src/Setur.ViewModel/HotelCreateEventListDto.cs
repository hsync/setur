﻿namespace Setur.ViewModel
{
    public class HotelCreateEventListDto
    {
        public IEnumerable<HotelCreateEventDto> Events { get; set; }
    }
}

﻿namespace Setur.ViewModel
{
    public class HotelCreateEventDto
    {
        public string App { get; set; }
        public string Type { get; set; }
        public DateTime Time { get; set; }
        public bool IsSucceeded { get; set; }
        public HotelMetaDto Meta { get; set; }
        public HotelUserDto User { get; set; }
        public Dictionary<string, string> Attributes { get; set; }
    }
}

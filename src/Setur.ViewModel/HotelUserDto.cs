﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Setur.ViewModel
{
    public class HotelUserDto
    {
        public string Id { get; set; }
        public bool IsAuthenticated { get; set; }
        public string Provider { get; set; }
        public string Email { get; set; }


    }
}
